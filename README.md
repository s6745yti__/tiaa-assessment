**How to run**

1. Unpack the package tiaa-coding.zip
2. Go to tiaa-coding folder
3. Open terminal and go to tiaa-coding folder
4. Run 'npm install' from the command line
5. Run 'npm start'
6. Stop 'npm start'
NOTE: (an error might show that's okay typescript is being setup initially)
7. Run 'npm start' again
6. The terminal output will show the results
