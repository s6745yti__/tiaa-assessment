"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var FileReader = /** @class */ (function () {
    function FileReader(filename) {
        this.filename = filename;
        this.data = undefined;
    }
    FileReader.prototype.read = function () {
        this.data = fs_1.default
            .readFileSync(this.filename, {
            encoding: 'utf-8'
        });
    };
    FileReader.prototype.dataToJson = function () {
        return JSON.parse(this.data);
    };
    return FileReader;
}());
exports.FileReader = FileReader;
