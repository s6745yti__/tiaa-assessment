"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.pick = function (object, whitelist) {
    var keys = Object.keys(object);
    var whiteListedKeys = keys.filter(function (key) { return whitelist.indexOf(key) !== -1; });
    return whiteListedKeys.reduce(function (filtered, key) {
        var _a;
        return Object.assign(filtered, (_a = {}, _a[key] = object[key], _a));
    }, {});
};
exports.simpleId = function () {
    return Math.floor(Math.random() * 100);
};
exports.flattenModelTargets = function (object) {
    var item;
    var result = [];
    for (var _i = 0, _a = Object.keys(object); _i < _a.length; _i++) {
        var key = _a[_i];
        item = object[key];
        if (Object.prototype.toString.call(item) === '[object Array]' && key === 'modelTargets') {
            item.forEach(function (data) {
                result.push(__assign({ parentId: object['currentName'], id: exports.simpleId() }, exports.pick(data, ['currentName', 'currentTarget', 'priorName', 'priorTarget', 'modelType'])));
                result = result.concat(exports.flattenModelTargets(data));
            });
        }
    }
    return result;
};
