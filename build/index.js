"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var FileReader_1 = require("./FileReader");
var utils_1 = require("./utils");
var parentThatChanged = [];
var results;
var reader = new FileReader_1.FileReader('data.json');
reader.read();
var flattenedData = utils_1.flattenModelTargets(reader.dataToJson());
console.log('***************************************' + '\n');
console.log('**flattened**' + '\n', flattenedData);
console.log('***************************************' + '\n');
var changedData = flattenedData.map(function (obj) {
    if (obj.currentTarget !== obj.priorTarget) {
        parentThatChanged.push(obj.parentId);
        return __assign(__assign({}, obj), { changed: true });
    }
    return obj;
});
parentThatChanged.map(function (parent) {
    results = changedData.map(function (obj) {
        if (!obj.hasOwnProperty('changed') && obj.parentId === parent) {
            return __assign(__assign({}, obj), { changed: true });
        }
        return obj;
    });
});
console.log('***************************************' + '\n');
console.log('**results**' + '\n', JSON.stringify(results, null, 2));
console.log('***************************************' + '\n');
