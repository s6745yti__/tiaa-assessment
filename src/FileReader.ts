import fs from 'fs';

export class FileReader{
    data: any = undefined;

    constructor(public filename: string) {}

    read(): void {
        this.data = fs
            .readFileSync(this.filename, {
                encoding: 'utf-8'
            });
    }
    dataToJson(): any {
      return JSON.parse(this.data);
    }
}
