
export interface ModelTargets {
    currentName: string;
    currentTarget: string;
    priorName: string;
    priorTarget: string;
    modelType: string;
}

export const pick = (object: Object, whitelist: any[]): any => {
    const keys = Object.keys(object);
    const whiteListedKeys = keys.filter(key => whitelist.indexOf(key) !== -1);

    return whiteListedKeys.reduce(
        (filtered: any, key: string): any =>
            Object.assign(filtered, { [key]: object[key] }),
        {}
    );
};

export const simpleId = () => {
    return Math.floor(Math.random() * 100);
};

export const flattenModelTargets = (object: Object): any[] => {
    let item: any;
    let result: any[] = [];
    for (const key of Object.keys(object)) {
        item = object[key];
        if (Object.prototype.toString.call(item) === '[object Array]' && key === 'modelTargets') {
            item.forEach( (data: any) => {
                result.push({parentId: object['currentName'],id: simpleId() ,...pick(data, ['currentName', 'currentTarget', 'priorName', 'priorTarget', 'modelType'])});
                result = result.concat(flattenModelTargets(data));
            });
        }
    }

    return result;

};


