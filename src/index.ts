import {FileReader} from './FileReader';
import {flattenModelTargets} from './utils';

const parentThatChanged: any = [];
let results;
const reader = new FileReader('data.json');
reader.read();

const flattenedData = flattenModelTargets(reader.dataToJson());
console.log('***************************************' + '\n');
console.log('**flattened**' + '\n', flattenedData);
console.log('***************************************' + '\n');
const changedData = flattenedData.map(obj=>{
    if (obj.currentTarget !== obj.priorTarget) {
        parentThatChanged.push(obj.parentId);
        return { ...obj, changed: true };
    }
    return obj;
});

parentThatChanged.map ((parent: any) => {
    results = changedData.map (obj => {
        if (!obj.hasOwnProperty('changed') && obj.parentId === parent) {
            return { ...obj, changed: true };
        }
        return obj;
    })
});
console.log('***************************************' + '\n');
console.log('**results**' + '\n', JSON.stringify(results, null, 2));
console.log('***************************************' + '\n');

